let number1 = parseInt(prompt("Enter first integer:"));
let number2 = parseInt(prompt("Enter second integer:"));
let operation = prompt("Enter operation(+ , - , * , / :");

let flag = 1;
  while (flag == 1) {
      if (isNaN(number1) || number1 == "") {
          number1 = parseInt(prompt("Make sure you entered  number for first integer please:"));
      }

      if(isNaN(number2) || number2 == ""){
          number2 = parseInt(prompt("Make sure you entered  number for second integer please:"));
      }

     if(!["+","-","*","/"].includes(operation)){
        operation = prompt("Only +,-,*,/ can be entered:");
     }

     else{
         flag = 0;
     }
  }

function calculateSum(int1,int2,ope) {
    let result = 0;
    if (ope == "+"){
        result = int1 + int2;
    }
    else if (ope == "-"){
        result = int1 - int2;
    }
    else if(ope == "*"){
        result = int1 * int2;
    }
    else if (ope == "/"){
        if (int2 == 0){
            result = "You can't divide by zero"
        }
        else{
            result = int1 / int2;
        }
    }
    return result;
}

console.log(calculateSum(number1,number2,operation));